## Dictation

A simple dictation app.

It grabs a random quote from quotesondesign.com and reads it aloud using [IBM Watson text-to-speech](https://www.ibm.com/watson/developercloud/text-to-speech.html) service. Type what you hear and check your memory and spelling.

![sample screenshot](https://pp.userapi.com/c836722/v836722046/52f13/yWfAmYIsM5I.jpg "sample screenshot")

## How to start:
1) get a watson TTS user and password, put it in a file 'keys.json' (take a look at the keys.example.json)
   (you can get register a free account here: https://www.ibm.com/watson/developercloud/text-to-speech.html)

2) npm install
3) npm run start
4) open https://localhost:8089/public/index.html


## TODOs:
1) Make a start easier.
2) Watson token is temporary, and when it dies, watson sends 401-response with header WWW-Authenticate. This makes browser to show default user/password popup. We have to avoid this.
3) Need to use other sources of sentences, not just quotes.
4) Pick quotes not only by length but by complexity.
5) Figure out a way to calculate sentence complexity.
4) Try and use other text-to-speech services.
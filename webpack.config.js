var isProduction = false;
for (var i = 2; i < process.argv.length; i++) {
  if (process.argv[i] === '-p') {
    isProduction = true;
  }
}
module.exports = require("./webpack.config.get")(isProduction);
var path = require('path');
var es3ifyPlugin = require('es3ify-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var webpack = require('webpack');
var WebpackCleanupPluginExports = require('webpack-cleanup-plugin');

module.exports = function(isProduction, externalOptions) {

  var keys = require('./keys.json');

  var plugins = [
    new WebpackCleanupPluginExports(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(isProduction ? 'production' : 'development'),
      VERSION: JSON.stringify(require("./package.json").version),
      THEYSAIDSO_API_KEY: JSON.stringify(keys.quotes)
    }),
    new ExtractTextPlugin("common.css"),
    new es3ifyPlugin()
  ];

  if (isProduction)
    plugins.unshift(new webpack.optimize.UglifyJsPlugin());

  var entries = {
    index: ['./src/index.tsx'],
  };

  var config = {
    context: __dirname,
    entry: entries,
    output: {
      path: __dirname + '/dist/',
      publicPath: '/dist/',
      filename: '[name].js'
    },
    resolve: {
      extensions: ['.ts', '.tsx', '.js', '.jsx'],
    },
    module: {

      rules: [
        { test: /\.tsx?$/, loader: 'ts-loader' },
        {
          test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          loader: "url-loader?limit=10000&mimetype=application/font-woff" },
        {
          test: /\.(ttf|eot|jpe?g|png|gif|svg)(\?v=[a-z0-9]\.[a-z0-9]\.[a-z0-9])?$/,
          loaders: ['url-loader?limit=10000', 'file-loader']
        },
        {
          test: /\.(css|less)$/,
          loader: ExtractTextPlugin.extract({
              fallback: 'style-loader',
              use: 'css-loader?sourceMap&localIdentName=[path][name]-[local]',
            }
          )
        },
        {
          test: /\.jsx?$/,
          loader: 'babel-loader',
          query: {
            presets: ['es2015', 'stage-0', 'react']
          },
          include: /retail-ui/
        },

      ]
    },

    plugins: plugins
  };


  return config;
};



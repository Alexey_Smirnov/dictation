const https = require('https');
const fs = require('fs');
import * as axios from 'axios';
var path = require('path');


let options = {
  key: fs.readFileSync(path.join(__dirname, './certs/key.pem')),
  cert: fs.readFileSync(path.join(__dirname, './certs/cert.pem'))
};

let keys = require('../keys.json');
let watsonTtsUser = keys.watson_tts_user;
let watsonTtsPassword = keys.watson_tts_password;
let authApiUrl = 'https://stream.watsonplatform.net/authorization/api/v1/';

https.createServer(options, (req, res) => {
  let url = `${authApiUrl}token?url=https://stream.watsonplatform.net/text-to-speech/api`;
  let base64 = new Buffer(watsonTtsUser + ':' + watsonTtsPassword).toString('base64');

  let authHeaderValue = 'Basic ' + base64;
  let config = {
    headers: { 'authorization': authHeaderValue },
    withCredentials: true,
  };

  axios.default.get(url, config).then(response => {
    res.setHeader("Access-Control-Allow-Origin", '*');
    res.setHeader("Access-Control-Allow-Methods", 'GET');
    res.writeHead(200);
    res.end(response.data.token);
  })
}).listen(3000);

console.log('https server started on port 3000');

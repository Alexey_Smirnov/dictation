import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {App} from './App';
import {observable} from 'mobx';
import './index.css';
import {AppState} from "./appState";


let appState = observable(new AppState());
(window as any).state = appState;
ReactDOM.render(<App app={appState}/>, document.getElementById('root'));





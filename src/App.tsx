import * as React from 'react';
import {observer} from "mobx-react";
import {QuoteFiltersComponent} from "./ui/filters";
import {AppState} from "./appState";
import {TextInput} from "./ui/inputs/text";
import {getDiff, generateHtmlDiff} from "./ui/htmlDiffGenerator";
import {IReadOptions, IVoice, SpeechProviderFactory} from "./speaker/shared";
import {random} from "./utils";
import {QuoteProviderFactory} from "./quotes/shared";

const Button = require("@skbkontur/react-ui/components/Button");
const Input = require("@skbkontur/react-ui/components/Input");
const Spinner = require("@skbkontur/react-ui/components/Spinner");

require('./app.css');
let voicesFile = require('./speaker/voices.json');
let voices = voicesFile.voices as IVoice[];


export interface IAppProps {
  app: AppState;
}


@observer
export class App extends React.Component<IAppProps, any> {
  componentDidMount() {
    document.getElementById('userInput').focus();
    return this.loadNext();
  }

  render() {
    return (
      <div className='App'>
        <div className="wrapper">
          <QuoteFiltersComponent className="box filters" filters={this.props.app.quoteFilters}/>

          <div className="box userInput">
            <TextInput id="userInput"
                       placeholder="Press 'Play' and type what you hear"
                       maxLength={300}
                       width='100%'
                       valueProxy={this.props.app.userInputText} onKeyDown={(e) => this.userInputOnKeyPress(e)}/>
          </div>
          <div className="box checkButtonCell">
            <Button use="success" width="100%" onClick={()=>this.buildDiff()}>Check</Button>
          </div>

          <div className="box audioControl">
            <audio id="audio" controls={true} width="100%" src={this.props.app.audioSrc} autoPlay={true}>
              Your browser does not support the audio element.
            </audio>
          </div>

          <div className="box loader">
            <span style={ {display: this.props.app.loader.isLoading.value ? 'block': 'none'}}>
              <Spinner type='mini' caption={this.props.app.loader.message.value} />
            </span>
          </div>
          <div className="box loadmore">
            <Button use="link" onClick={()=>this.loadNext()}>Load next</Button>
          </div>

          <div className="box result">
            <div style={{display: this.props.app.isDiffVisible.value ? 'block': 'none'}}>
              <span>{this.props.app.quoteText.value}</span>
              <div id="diffTargetId"/>
            </div>
          </div>
        </div>
        Controls:
        <ul>
          <li>Press DownArrow to play audio</li>
          <li>Press Enter to check</li>
          <li>Press Enter again (or up arrow) to move on to the next quote</li>
        </ul>
      </div>
    );
  }

  private buildDiff() {
    let target = document.getElementById('diffTargetId');

    let diff = getDiff(this.props.app.quoteText.value, this.props.app.userInputText.value);
    generateHtmlDiff(target, diff);
    this.props.app.isDiffVisible.value = true;
  }

  private loadNext() {
    this.props.app.isDiffVisible.value = false;
    this.props.app.audio.value = null;
    this.props.app.userInputText.value = '';
    this.props.app.quoteText.value = '';

    this.props.app.loader.isLoading.value = true;
    this.props.app.loader.message.value = 'loading quote';

    let quoteProvider = QuoteProviderFactory.instance();
    let audioProvider = SpeechProviderFactory.instance();
    let minLength = this.props.app.quoteFilters.minLength.value;
    let maxLength = this.props.app.quoteFilters.maxLength.value;

    return quoteProvider.get(minLength, maxLength).then(quote => {
      this.props.app.loader.message.value = 'loading quote';
      this.props.app.quoteText.value = quote.text;
      let readOptions: IReadOptions = {
        randomizePitch: this.props.app.quoteFilters.randomizePitch.value,
        randomizeRate: this.props.app.quoteFilters.randomizeRate.value,
        voice: random(voices).name
      };

      console.log('Voice: ' + readOptions.voice);

      return audioProvider.read(quote.text, readOptions).then(speech => {
        this.props.app.audio.value = speech.data;
        this.props.app.loader.isLoading.value = false
      })
    });
  }

  private userInputOnKeyPress(e) {
    let enterKey = 13;
    let upArrow = 38;
    let downArrow = 40;

    let hasPressed = (code) => e.which == code || e.keyCode == code;
    if (hasPressed(enterKey)) {
      if (this.props.app.isDiffVisible.value) {
        this.loadNext();
      } else {
        this.buildDiff();
      }
      return false;
    } else if (hasPressed(upArrow)) {
      this.loadNext();
      return false;
    } else if (hasPressed(downArrow)) {
      let audioElement = document.getElementById('audio') as HTMLAudioElement;
      audioElement.currentTime=0;
      audioElement.play();
    }
  }

}

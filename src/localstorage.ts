export class LocalStorage {
  static set(name,value,days) {
    let expires = "";

    if (days) {
      const date = new Date();
      date.setTime(date.getTime()+ (days*24*60*60*1000));
      let expires = "; expires="+date.toUTCString();
    }

    if(this.localStoreSupport() ) {
      localStorage.setItem(name, value);
    }
    else {
      document.cookie = name+"="+value+expires+"; path=/";
    }
  }

  static get(name) {
    if( this.localStoreSupport() ) {
      var ret = localStorage.getItem(name);
      //console.log(typeof ret);
      switch (ret) {
        case 'true':
          return true;
        case 'false':
          return false;
        default:
          return ret;
      }
    }
    else {
      const nameEQ = name + "=";
      const ca = document.cookie.split(';');
      for(let i=0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) {
          ret = c.substring(nameEQ.length,c.length);
          switch (ret) {
            case 'true':
              return true;
            case 'false':
              return false;
            default:
              return ret;
          }
        }
      }
      return null;
    }
  }

  static delete(name) {
    if(this.localStoreSupport() ) {
      localStorage.removeItem(name);
    }
    else {
      this.set(name,"",-1);
    }
  }

  private static localStoreSupport() {
    try {
      return 'localStorage' in window && window['localStorage'] !== null;
    } catch (e) {
      return false;
    }
  }
}

import {observable, computed} from 'mobx';


export class ValueWrapper<T> {
  @observable public value: T;
  constructor(value: T) {
    this.value = value;
  }
}

export class AppState {
  @observable readonly quoteFilters = new QuoteFilters();
  @observable readonly loader = new QuoteLoaderState();

  @observable readonly quoteText = new ValueWrapper('');
  @observable readonly audio = new ValueWrapper<Blob>(null);
  @observable readonly userInputText = new ValueWrapper('');
  @observable readonly isDiffVisible = new ValueWrapper(false);

  @computed get audioSrc(): string {
    return this.audio.value ? window.URL.createObjectURL(this.audio.value) : null;
  }
}



export class QuoteFilters {
  @observable readonly minLength? = new ValueWrapper(40);
  @observable readonly maxLength? = new ValueWrapper(80);
  @observable readonly randomizeRate? = new ValueWrapper(false);
  @observable readonly randomizePitch? =new ValueWrapper(false);
}

export class QuoteLoaderState {
  @observable readonly isLoading = new ValueWrapper(false);
  @observable readonly message = new ValueWrapper('loading');
}

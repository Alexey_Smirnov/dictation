export function setUrlParam(url, param, value) {
  let sep = url.indexOf('?') >= 0 ? '&' : '?';
  url = url.replace(new RegExp(param + '\=.*?(\&|$)', 'g'), '');
  url = url + sep + param + '=' + encodeURIComponent(value);
  url = url.replace('&&', '&');
  url = url.replace('?&', '?');
  return url;
}

export function promiseResolve<T>(value?: T): Promise<T> {
  return new Promise<T>((resolve, reject) => resolve(value));
}

export function random<T>(collection: T[]): T {
  return collection[Math.floor(Math.random()*collection.length)];
}

export function getRandomInt(min: number, max: number): number {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function base64toBlob(b64Data, contentType?, sliceSize?): Blob {
  contentType = contentType || '';
  sliceSize = sliceSize || 512;

  let byteCharacters = atob(b64Data);
  let byteArrays = [];

  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    let slice = byteCharacters.slice(offset, offset + sliceSize);

    let byteNumbers = new Array(slice.length);
    for (let i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }
    let byteArray = new Uint8Array(byteNumbers);
    byteArrays.push(byteArray);
  }

  return new Blob(byteArrays, {type: contentType});
}


export function retry<T>(f: () => Promise<T>, attempts, shouldRetryPredicate?: (r: T) => boolean): Promise<T> {
  shouldRetryPredicate = shouldRetryPredicate ? shouldRetryPredicate : (r) => false;

  let tryResolve = () => {
    attempts--;
    let retryMessage = 'shouldRetryPredicate returned true, f() result is not good enough.';

    return f()
      .then(r => shouldRetryPredicate(r) ? Promise.reject<T>(new Error(retryMessage)) : r)
      .then(null, e => {
        if (attempts == 0) {
          return Promise.reject<T>(new Error(`${e.message}. Max attempts reached.`));
        }
        return tryResolve();
      });
  };
  return tryResolve();
}

import {IQuote, IQuoteProvider} from "./shared";
import axios from 'axios';
import {promiseResolve} from "../utils";

export class QuotesOnDesignProvider implements IQuoteProvider {
  private cache = {};

  get(minLength?: number, maxLength?: number): Promise<IQuote> {
    let cacheKey = `${minLength}_${maxLength}`;

    let fillCachePromise = promiseResolve();
    if (!this.cache[cacheKey] || !this.cache[cacheKey].length)
      fillCachePromise = this.getInner(minLength, maxLength).then(quotes => this.cache[cacheKey] = quotes);

    return fillCachePromise.then(() => {
      return this.cache[cacheKey].pop()
    });
  }

  private getInner(minLength?: number, maxLength?: number): Promise<IQuote[]> {
    let url = `https://quotesondesign.com/wp-json/posts?filter[orderby]=rand&filter[posts_per_page]=40`;
    return axios.get(url).then(response => {
      let quotes = this.stripOfHtml(response.data);
      if (minLength)
        quotes = quotes.filter(q => q.text.length > minLength);

      if (maxLength)
        quotes = quotes.filter(q => q.text.length < maxLength);
      return quotes;
    });
  }

  private stripOfHtml(rawQuotes: IQuoteOnDesignDto[]): IQuote[] {
    let element = document.createElement('span');
    return rawQuotes.map(q => {
      element.innerHTML = q.content;
      return <IQuote> {
        text: element.innerText,
        author: q.title
      }
    });
  }

}

interface IQuoteOnDesignDto {
  content: string;
  title: string;
}

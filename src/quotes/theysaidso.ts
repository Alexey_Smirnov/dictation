import axios, {AxiosRequestConfig, AxiosResponse} from 'axios';
import {retry, setUrlParam} from "../utils";
import {IQuote, IQuoteProvider} from "./shared";


export class TheySaidSoQuoteProvider implements IQuoteProvider {
  private cache = {};

  constructor(private apiKey: string) {
  }

  get(minLength?: number, maxLength?: number): Promise<IQuote> {
    let key = `${minLength}_${maxLength}`;
    let value = this.cache[key] as Promise<IQuote>;
    this.cache[key] = null;

    let valuePromise = value ? value : this.send(minLength, maxLength);

    return valuePromise.then(q => {
      this.cache[key] = this.send(minLength, maxLength);
      return q;
    });

  }

  private send(min?: number, max?: number): Promise<IQuote> {
    let url = `https://quotes.rest/quote/search.json`;
    if (min)
      url = setUrlParam(url, 'minlength', min);
    if (min)
      url = setUrlParam(url, 'maxlength', max);

    let requestConfig = <AxiosRequestConfig> {headers: { 'X-Theysaidso-Api-Secret': this.apiKey }};

    let promise = () => axios.get(url, requestConfig);
    let shouldRetryPredicate = (r: AxiosResponse) => {
      if (!r.data || r.status != 200) {
        console.log("TheySaidSo messed up response: " + r.status);
        return true;
      }

      let quote = r.data.contents.quote;
      let actualLength = quote.length;

      if ((min && actualLength < min) || (max && actualLength > max)) {
        console.log("TheySaidSo messed up length: " + actualLength);
        return true;
      }

      return false;
    };

    return retry(promise, 50, shouldRetryPredicate).then((response: AxiosResponse) => {
      return <IQuote>{
        text: response.data.contents.quote,
        author: response.data.contents.author
      };
    })
  }
}

import {TheySaidSoQuoteProvider} from "./theysaidso";
import {QuotesOnDesignProvider} from "./quotesondesign";
import {TextfilesProvider} from "./textfiles";

export interface IQuoteProvider {
  get(minLength?: number, maxLength?: number): Promise<IQuote>;
}

export interface IQuote {
  text: string;
  author?: string;
}

declare let THEYSAIDSO_API_KEY: string;
let providerInstance: IQuoteProvider;


export class QuoteProviderFactory {
  static instance(): IQuoteProvider {
    if (providerInstance)
      return providerInstance;

    // return providerInstance = new TheySaidSoQuoteProvider(THEYSAIDSO_API_KEY);
    // return providerInstance = new QuotesOnDesignProvider();

    return providerInstance = new TextfilesProvider();
  }
}

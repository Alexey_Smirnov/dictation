import {IQuote, IQuoteProvider} from "./shared";
import {promiseResolve} from "../utils";


export class StubQuoteProvider implements IQuoteProvider {
  private quotes = [
    `JS had to “look like Java” only less so, be Java’s dumb kid brother or boy-hostage sidekick. Plus, I had to be done in ten days or something worse than JS would have happened.`,
    `People will always want to do stupid things, and luckily for those people, there's JSHint`,
    `JavaScript is to Java as hamster is to ham.`,
    `Learning JavaScript used to mean you weren't a serious software developer. Today, not learning Javascript means the same thing.`,
    `Too much syntactic sugar causes cancer of the semi-colon.`
  ];

  get(minLength?: number, maxLength?: number): Promise<IQuote> {

    let filtered = this.quotes;
    if (minLength)
      filtered = filtered.filter(q => q.length > minLength);
    if (maxLength)
      filtered = filtered.filter(q => q.length < maxLength);

    let text = filtered[Math.floor(Math.random()*filtered.length)];

    let value = text ? {text: text} : null;
    return promiseResolve<IQuote>(value);
  }

}

import {IQuote, IQuoteProvider} from "./shared";
import {promiseResolve, random} from "../utils";
import axios, {AxiosRequestConfig, AxiosResponse} from 'axios';


let textFileNames = require('./textfilesLinks.json');
let files = textFileNames.files;

export class TextfilesProvider implements IQuoteProvider {
  private baseUrl = 'https://cors-anywhere.herokuapp.com/http://www.textfiles.com/etext/FICTION/';
  // private baseUrl = 'https://www.textfiles.com/etext/FICTION/';

  private cache : {[key: string]: string[]} = {};
  private initPromise: Promise<any>;

  constructor() {
    this.initPromise = this.init();
  }

  get(minLength?: number, maxLength?: number): Promise<IQuote> {

    let keys = Object.keys(this.cache);
    let keyPromise = keys.length
      ? promiseResolve(random(keys))
      : this.initPromise.then(() => random(Object.keys(this.cache)));

    return keyPromise.then(key => {
      console.log('Reading ' + key);
      let sentences = this.cache[key];

      if (minLength && maxLength) sentences = sentences.filter(s => s.length >= minLength && s.length <= maxLength);
      else if (minLength) sentences = sentences.filter(s => s.length >= minLength);
      else if (maxLength) sentences = sentences.filter(s => s.length <= maxLength);

      return <IQuote> {
        text:random(sentences),
      };
    });

  }

  private init() {
    return new Promise((resolve, reject) => {
      for (let i=0; i<3; i++) {
        let filename = random(files) as string;

        // resolve big promise, but continue to handle the rest
        // swallow any errors
        this.sortSentences(filename).then(r => {
          this.cache[filename] = r;
          console.log(`Filename: ${filename} saved to cache.`);
          resolve();
        }, e => {})
      }
    })
    .then(()=> console.log('inited'));
  }

  // total outrage, but hey, it does the job
  private sortSentences(filename): Promise<string[]> {
    let config = <AxiosRequestConfig> {
      headers: {'Origin': 'https://localhost:8089'},
      withCredential: true,
    };

    return axios.get(this.baseUrl + filename, config).then((r: AxiosResponse) => {

      // fixing paragraps and chapter names
      let phase1 = r.data.replace(/\n\n/g, '. ');

      // removing all new-lines
      let phase2 = phase1.replace(/[\r\n]/g, ' ');

      // removing extra spaces
      let phase3 = phase2.replace(/ +(?= )/g,'');

      // js does not support look-behind assertions
      let phase4 = phase3.replace(/Mr\./g, 'Mr').replace(/Mrs\./g, 'Mrs').replace(/Ms\./g, 'Ms').replace(/Dr\./g, 'Dr');

      // matching sentences
      let sentences = phase4.match(/[A-Z].*?[.?!]/gm);

      // filtering junk sentences and sentences with reported speech
      let filtered = sentences.filter(s => s.indexOf('"') < 0 && s.length > 20);

      console.log(`Filename: ${filename}, sentences count: ${filtered.length}.`);
      return filtered.sort((arg1, arg2) => arg1.length - arg2.length);
    });
  }

}

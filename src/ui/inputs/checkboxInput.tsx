import * as React from 'react';
import {observer} from "mobx-react";
const Checkbox = require("@skbkontur/react-ui/components/Checkbox");

@observer
export class CheckboxInput extends React.Component<any, any> {
  render() {
    return <Checkbox checked={this.props.valueProxy.value} onChange={this.handleChange.bind(this)}>
      {this.props.text}
    </Checkbox>;
  }

  private handleChange(e) {
    this.props.valueProxy.value = e.target.checked;
  }
}

import * as React from 'react';
import {observer} from "mobx-react";
const Input = require("@skbkontur/react-ui/components/Input");

@observer
export class TextInput extends React.Component<any, any> {
  render() {
    return <div>
      {this.props.label && <label>{this.props.label}: </label>}
      <Input
        ref="input"
        maxLength={3}
        width='50px'
        value={this.props.valueProxy.value + ''}
        onChange={(e)=> {
          this.props.valueProxy.value = e.target.value;
        }}
        {...this.props}
      />
      </div>;
  }
}


@observer
export class NumberInput extends React.Component<any, any> {
  render() {
    return <div>
      {this.props.label && <label>{this.props.label}: </label>}
      <Input
        ref="input"
        maxLength={3}
        width='50px'
        value={this.props.valueProxy.value + ''}
        onChange={(e)=> {
          this.props.valueProxy.value = Number(e.target.value);
        }}
        {...this.props}
      />
    </div>;
  }
}

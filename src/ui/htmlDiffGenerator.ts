import {diffWordsWithSpace, IDiffResult} from 'diff';

export function generateHtmlDiff(target: HTMLElement, diff: IDiffResult[]) {
  let color = '';
  let span = null;

  let fragment = document.createDocumentFragment();
  diff.forEach(part => {
    // green for additions, red for deletions
    // grey for common parts
    color = part.added ? 'green' :
      part.removed ? 'red' : 'grey';
    span = document.createElement('span');
    span.style.color = color;
    span.appendChild(document.createTextNode(part.value));
    fragment.appendChild(span);
  });

  target.innerHTML = '';
  target.innerText = '';
  target.appendChild(fragment);
}


export function getDiff(first: string, second: string): IDiffResult[] {
  return diffWordsWithSpace(first, second);
}

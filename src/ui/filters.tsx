import * as React from 'react';
import {observer} from "mobx-react";
import {CheckboxInput} from "./inputs/checkboxInput";
import {NumberInput, TextInput} from "./inputs/text";


@observer
export class QuoteFiltersComponent extends React.Component<any, any> {

  render() {
    return <div className={this.props.className}>
      <NumberInput label="min length" valueProxy={this.props.filters.minLength} />
      <NumberInput label="max length" valueProxy={this.props.filters.maxLength} />
      <CheckboxInput text="randomize pitch" valueProxy={this.props.filters.randomizePitch}/>
      <CheckboxInput text="randomize rate" valueProxy={this.props.filters.randomizeRate}/>
    </div>
  }
}

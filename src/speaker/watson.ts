import {canPlayAudioFormat, getSearchParams, IReadOptions, ISpeech, ISpeechProvider} from "./shared";
import {getRandomInt, promiseResolve} from "../utils";
import {LocalStorage} from "../localstorage";


export class WatsonSpeechProvider implements ISpeechProvider {
  constructor(private ttsApiUrl: string) {
  }

  read(text: string, options: IReadOptions): Promise<ISpeech> {
    let authPromise = this.watsonToken() ? promiseResolve() : this.authorize();

    return authPromise.then(() => {
      let url = this.buildRequestUrl(text, options);
      let func = () => this.readInner(url);

      return func().then(null, e => {
        if (e.message = 'unauthorized')
          return this.authorize().then(() => func());
        return Promise.reject<ISpeech>(e);
      });

    })
  }


  private readInner(url: string): Promise<ISpeech> {
    return fetch(url, this.getRequestConfig()).then((response) => {
      if (response.ok) {
        return response.blob().then(blob => <ISpeech>{data: blob});
      } else {
        if (response.status == 401) {
          this.watsonToken = null;
          return Promise.reject<ISpeech>(new Error("unauthorized"));
        }

        return response.json().then(json => {
          let error = new Error("Failed to synthesize speech: " + JSON.stringify(json));
          return Promise.reject<ISpeech>(error)
        });
      }
    });
  }

  private buildRequestUrl(text: string, options: IReadOptions): string {
    let transformed = this.setTransformations(text, options);
    let params = this.getSynthParams(transformed, options.voice);
    return `${this.ttsApiUrl}synthesize?${params.toString()}`;
  }

  private setTransformations(text: string, options: IReadOptions): string {
    let pitchValue = getRandomInt(-6, 6);
    let rateValue = getRandomInt(-10, 25);

    let pitch = options.randomizePitch && pitchValue != 0 ? `pitch='${pitchValue > 0 ? '+' : ''}${pitchValue}st'` : '';
    let rate = options.randomizeRate && rateValue != 0 ? `rate='${rateValue > 0 ? '+' : ''}${rateValue}%'` : '';

    if (!options.randomizeRate && !options.randomizeRate)
      return text;
    return `<speak version="1.0"><prosody ${pitch} ${rate}>${text}</prosody></speak>`;
  }

  private getSynthParams(text: string, voice: string): any {
    let params = getSearchParams();
    params.set('text', text);
    params.set('voice', voice);
    if (!canPlayAudioFormat('audio/ogg;codec=opus') && canPlayAudioFormat('audio/wav')) {
      params.set('accept', 'audio/wav');
    }
    return params;
  }

  private authorize(): Promise<string> {
    let url = `https://localhost:3000/`;
    return fetch(url).then(response => response.text()).then(text => this.watsonToken(text))
  }

  private getRequestConfig(): RequestInit {
    return {
      headers: { 'X-Watson-Authorization-Token': this.watsonToken() },
      mode: "cors",
      credentials: "include"
    };
  }

  private watsonToken(value?: string): string {
    if (value) {
      LocalStorage.set('watsonToken', value, 1);
      return value;
    }
    return LocalStorage.get('watsonToken') as string;
  }

}

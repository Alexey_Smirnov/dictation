import {WatsonSpeechProvider} from "./watson";

export function canPlayAudioFormat (mimeType) {
  let audio = document.createElement('audio');
  if (audio) {
    return (typeof audio.canPlayType === 'function' && audio.canPlayType(mimeType) !== '');
  } else {
    return false
  }
}

/**
 * @return {Function} A polyfill for URLSearchParams
 */
export function getSearchParams() {
  if (typeof URLSearchParams === 'function') {
    return new URLSearchParams();
  }

  //simple polyfill for URLSearchparams
  let searchParams = function () {
  };

  searchParams.prototype.set = function (key, value) {
    this[key] = value;
  };

  searchParams.prototype.toString = function () {
    return Object.keys(this).map(function (v) {
      return `${encodeURI(v)}=${encodeURI(this[v])}`;
    }.bind(this)).join("&");
  };
  return new searchParams();
}

export interface IReadOptions {
  voice: string;
  randomizePitch?: boolean;
  randomizeRate?: boolean;
}

export interface ISpeech {
  data: Blob;
}

export interface IVoice {
  name: string;
  language: string;
  customizable: boolean;
  gender: VoiceGender;
  url: string;
  description: string;
  supported_features: ISupportedVoiceFeatures
}

export type VoiceGender = 'male' | 'female';
export class VoiceGenders {
  static Male: VoiceGender = 'male';
  static Female: VoiceGender = 'female';
}

export interface ISupportedVoiceFeatures {
  voice_transformation: boolean;
  custom_pronunciation: boolean;
}


export interface ISpeechProvider {
  read(text: string, options: IReadOptions): Promise<ISpeech>;
}


let provider: ISpeechProvider;

export class SpeechProviderFactory {
  static instance(): ISpeechProvider {
    if (provider)
      return provider;

    let ttsApiUrl = 'https://stream.watsonplatform.net/text-to-speech/api/v1/';
    return provider = new WatsonSpeechProvider(ttsApiUrl);
  }
}
